const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const process = require('process')
// const FaviconsWebpackPlugin = require('favicons-webpack-plugin')
// const { CleanWebpackPlugin } = require('clean-webpack-plugin');
const TerserPlugin = require("terser-webpack-plugin");
const Visualizer = require('webpack-visualizer-plugin2');
const { StatsWriterPlugin } = require("webpack-stats-plugin")
const webpack = require('webpack')

const production = process.env["CI"] === "true" || process.env["PROD"] === "true"
const devMode = !production

let config = {
  mode: devMode ? "development" : "production",
  devtool: devMode ? 'inline-cheap-module-source-map' : undefined,
  devServer: devMode ? {
    contentBase: './dist',
    disableHostCheck: true,
    host: '0.0.0.0',
  } : undefined,
  output: {
    filename: 'aframe-enviropacks.js',
    path: path.resolve(__dirname, 'dist'),
  },
  module: {
    rules: [
      { test: /\.html$/, loader: 'html-loader' },
      { test: /oss-licenses-used/,loader: 'raw-loader'},
      { test: /\.(md)$/, loader: 'html-loader!markdown-loader' },
      { test: /\.(frag|vert|glsl)$/, loader: 'glsl-shader-loader'},
      { test: /\.(styl)$/, loader: 'style-loader!css-loader!stylus-loader'},
      { test: /gallery.*\.vartistez?$/,
        use: [{
          loader: 'file-loader',
          options: {
            name: 'gallery/[name].[ext]'
          }
        }]
      },
      {
        test: /\.worker\.js$/,
        use: { loader: "worker-loader" },
      },
      {
        test: /package.json/,
        type: 'javascript/auto',
        use: [{
          loader: 'file-loader',
          options: {
            esModule: false,
            name: '[name].[ext]'
          }
        }]
      },
      {test: /\.(png|jpe?g|gif|obj|mtl|glb|wav|hdr|webp)$/i,
        use: [
          {
            loader: 'url-loader',
            options: {
              limit: 1024,
              esModule: false,
              fallback: require.resolve('file-loader'),
              name: 'asset/[folder]/[name].[ext]',
            },
          },
        ]
      },
    ]
  }
};

const faviconPath = './src/static/favicon.png'

function minimizer() {
  if (devMode) return undefined;

  return [new TerserPlugin({
  parallel: true,
  terserOptions: {
    toplevel: true,
    keep_classnames: /Node|Layer/
  }
})]};

let app = Object.assign({
  entry: {
    app: './src/index.js'
  },
  plugins: [
    // new FaviconsWebpackPlugin(faviconPath),
    new HtmlWebpackPlugin({
      template: './src/demo.html',
      filename: 'demo.html',
      inject: 'head',
      scriptLoading: 'blocking',
    }),
  ].concat(devMode
    ? [
       // new webpack.SourceMapDevToolPlugin({
       //   exclude: /material-packs/,
       // })
    ]
    : [
    // new StatsWriterPlugin({
    //     filename: path.join('.', 'stats', 'app-log.json'),
    //     fields: null,
    //     stats: { chunkModules: true },
    // }),
    // new Visualizer({
    //     filename: path.join('.', 'stats', 'app-statistics.html'),
    // }),
  ]),
  optimization: {
    splitChunks: {},
    minimize: !devMode,
    minimizer: minimizer(),
  },
}, config);

module.exports = [app]
