#define MATCAP
uniform vec3 color;
uniform float opacity;
uniform sampler2D roughDialectric;
uniform sampler2D smoothDialectric;
uniform sampler2D roughMetallic;
uniform sampler2D smoothMetallic;
uniform float roughness;
uniform float metalness;

varying vec3 vViewPosition;
#ifndef FLAT_SHADED
	varying vec3 vNormal;
#endif
#include <common>
#include <dithering_pars_fragment>
#include <color_pars_fragment>
#include <uv_pars_fragment>
#include <map_pars_fragment>
#include <alphamap_pars_fragment>
#include <fog_pars_fragment>
#include <bumpmap_pars_fragment>
#include <normalmap_pars_fragment>
#include <logdepthbuf_pars_fragment>
#include <clipping_planes_pars_fragment>
#include <roughnessmap_pars_fragment>
#include <metalnessmap_pars_fragment>
void main() {
	#include <clipping_planes_fragment>
	vec4 diffuseColor = vec4( color, opacity );
	#include <logdepthbuf_fragment>
	#include <map_fragment>
	#include <color_fragment>
	#include <alphamap_fragment>
	#include <alphatest_fragment>
	#include <normal_fragment_begin>
	#include <normal_fragment_maps>
  #include <metalnessmap_fragment>
  #include <roughnessmap_fragment>
	vec3 viewDir = normalize( vViewPosition );
	vec3 x = normalize( vec3( viewDir.z, 0.0, - viewDir.x ) );
	vec3 y = cross( viewDir, x );
	vec2 uv = vec2( dot( x, normal ), dot( y, normal ) ) * 0.495 + 0.5; // 0.495 to remove artifacts caused by undersized matcap disks


  vec4 diaelectricColor = mix(texture2D( smoothDialectric, uv ),
                              texture2D( roughDialectric, uv ),
                              roughnessFactor);
  vec4 metalnessColor = mix(texture2D( smoothMetallic, uv ),
                            texture2D( roughMetallic, uv ),
                            roughnessFactor);

  vec4 matcapColor = mix(diaelectricColor, metalnessColor, metalnessFactor);
	/* matcapColor = matcapTexelToLinear( matcapColor ); */


	diffuseColor = pow(diffuseColor, vec4(1.0 / 1.1));
	vec3 outgoingLight = diffuseColor.rgb * matcapColor.rgb;// + metalnessColor.rgb * metalnessFactor * 0.5;
	gl_FragColor = vec4( outgoingLight, diffuseColor.a );

	/* gl_FragColor = vec4(roughnessFactor, roughnessFactor, roughnessFactor, 1.0); */
	/* gl_FragColor = vec4(normal, 1.0); */
	#include <tonemapping_fragment>
	#include <encodings_fragment>
	#include <fog_fragment>
	/* #include <premultiplied_alpha_fragment> */
	#include <dithering_fragment>
}
